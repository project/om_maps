<?php
// $Id$

/**
 * @file
 * OM Maps Admin Configuration
 *
 * @author: Daniel Honrade http://drupal.org/user/351112
 *
 */
 
/**
 * Admin Form - Simple Editing
 *
 */
function om_maps_admin() {
    
  drupal_set_title(t('OM Maps Settings'));

  // get country codes
  $menu_keys = array_keys(om_maps_check());

  // existing maps
  $maps = om_maps_get();

  $form = array();
  $form['om_maps'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
    '#description' => t('Click load button to upload map/s. These Map/s are automatically added to OM Maximenu.<br />
      (Once enabled, it can only be disabled by deleting it using the OM Maximenu settings.)
    
    
    '),
  );  
  
  $maps_array = array();  
  foreach ($maps as $map_name => $map_content) { 
    $code    = $map_content['code'];
    $enabled = in_array($code, $menu_keys) ? 1: 0;
    $title   = $enabled ? om_string_name($map_content['title'], FALSE) . ' (Enabled)': om_string_name($map_content['title'], FALSE);
    $maps_array[$map_name]['enabled'] = array(
      '#type'          => 'checkbox',
      '#title'         => $title,
      '#disabled'      => $enabled,
      '#default_value' => $enabled,
    ); 
   $form['om_maps'] += $maps_array;
  }
   
  $form['Load'] = array( '#type' => 'submit', '#value' => t('Load'), );
  
  return $form;
}


/**
 * 1 Submit for all settings
 *
 */
function om_maps_admin_submit($form, $form_state) {
  // all menus settings
  $om_maximenu = variable_get('om_maximenu', array());
  //dsm($maximenu);        

  $om_maps_array = $form_state['values']['om_maps'];
  //dsm($om_maps_array); 
  $count = !empty($om_maximenu) ? max(array_keys($om_maximenu)) + 1: 1;
  
  $i = 0;  
  $maps_array = array();  
  foreach ($om_maps_array as $map_name => $content) {
    if ($content['enabled'] == 1) {
      $count =  $count + $i;
      $map_content = om_maps_content_get($map_name);
      $om_maximenu[$count] = $map_content;
      $i++;
    } 
  }
  dsm($om_maximenu);
  drupal_set_message(t('Your map/s have been saved.'));
  
  // Save all settings in 1 variable
  variable_set('om_maximenu', $om_maximenu);
} 

