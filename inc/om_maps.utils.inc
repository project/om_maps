<?php
// $Id$

/**
 * @file
 * OM Maps Admin Utilities
 *
 * @author: Daniel Honrade http://drupal.org/user/351112
 *
 */
define('OM_MAPS_PATH', drupal_get_path('module', 'om_maps')); 

// OM Map css loader
om_maps_load_css();

/**
 * OM Maps directory
 * - reads directory names
 */
function om_maps_get() {
  module_load_include('inc', 'om_maximenu', 'inc/om_maximenu.utils');  
  $files = array();
  $dir = OM_MAPS_PATH . '/maps/';
  $folders = scandir($dir);
  $excluded_files = array('.', '..', '.cvs', '.svn', '.git');

  foreach ($folders as $key => $val) {
    if (!in_array($val, $excluded_files)) (is_dir($dir . $val)) ? $files[$val] = om_maps_content_get(om_string_name($val)): '';     
  }
  return $files;
}


/**
 * OM Maps contents
 * 
 */
function om_maps_content_get($map_name = NULL) {
  $map_content = array();
  if ($map_name) {
    $file_name = OM_MAPS_PATH . '/maps/' . $map_name . '/' . $map_name . '.inc';
    $map_load = file_get_contents($file_name);
    ob_start();
    eval($map_load);
    //$output = ob_get_contents();
    ob_end_clean(); 
    $map_content = $$map_name; 
  } 
  return $map_content;
}
  
  
/**
 * OM Maps check loaded
 * - reads map names in OM Maximenu variable
 */
function om_maps_check() {
  $om_maximenu = variable_get('om_maximenu', array());

  $menus = array();  
  foreach ($om_maximenu as $menu_key => $content) {
    $code = $content['code'];
    $menus[$code] = $content['title'];
  }
  return $menus;
}  
  
  
/**
 * OM Maps load css
 */
function om_maps_load_css() {
  // get country codes
  $menu_keys = array_keys(om_maps_check());

  // existing maps
  $maps = om_maps_get(); 
  
  $maps_array = array();  
  foreach ($maps as $map_name => $map_content) { 
    $code    = $map_content['code'];
    $enabled = in_array($code, $menu_keys) ? 1: 0;
    if ($enabled) drupal_add_css(OM_MAPS_PATH . '/maps/' . $map_name . '/' . $map_name . '.css');
  }  
}  
  
